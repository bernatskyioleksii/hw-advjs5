


// Отримання списку користувачів та публікацій з сервера
Promise.all([
	fetch('https://ajax.test-danit.com/api/json/users'),
	fetch('https://ajax.test-danit.com/api/json/posts')
])
	.then(responses => Promise.all(responses.map(response => response.json())))
	.then(data => {
		const [users, posts] = data;
		const newsFeed = document.getElementById('newsFeed');

		// Приховування анімації завантаження
		const loader = document.querySelector('.loader');
		loader.style.display = 'none';

		// Клас для створення картки
		class Card {
			constructor(postId, title, text, userName, userEmail) {
				this.postId = postId;
				this.title = title;
				this.text = text;
				this.userName = userName;
				this.userEmail = userEmail;
			}

			render() {
				// Створюємо HTML-структуру картки
				const cardElement = document.createElement('div');
				cardElement.className = 'card';
				cardElement.id = `card-${this.postId}`; // Додаємо ідентифікатор для легшого видалення

				const titleElement = document.createElement('h3');
				titleElement.textContent = this.title;

				const textElement = document.createElement('p');
				textElement.textContent = this.text;

				const userElement = document.createElement('p');
				userElement.textContent = `By ${this.userName} (${this.userEmail})`;

				const deleteButton = document.createElement('button');
				deleteButton.textContent = 'Delete';
				deleteButton.addEventListener('click', () => this.deleteCard());

				// Додаємо елементи до картки
				cardElement.appendChild(titleElement);
				cardElement.appendChild(textElement);
				cardElement.appendChild(userElement);
				cardElement.appendChild(deleteButton);

				return cardElement;
			}

			deleteCard() {
				// Відправляємо DELETE запит для видалення картки
				fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
					method: 'DELETE'
				})
					.then(response => {
						if (response.ok) {
							// Якщо запит пройшов успішно, видаляємо картку зі сторінки
							const cardElement = document.getElementById(`card-${this.postId}`);
							cardElement.remove();
						}
					})
					.catch(error => {
						console.error('Error deleting card:', error);
					});
			}
		}

		// Функція для створення та додавання картки публікації до стрічки новин
		function createPostCard(post) {
			// Знаходимо користувача, якому належить публікація
			const user = users.find(user => user.id === post.userId);

			if (user) {
				const card = new Card(post.id, post.title, post.body, user.name, user.email);
				const cardElement = card.render();

				// Додаємо картку до контейнера
				newsFeed.prepend(cardElement); // Додавання зверху сторінки
			}
		}

		// Рендеринг карток публікацій
		posts.forEach(post => {
			createPostCard(post);
		});

		// Показ модального вікна при натисканні на кнопку "Додати публікацію"
		const addPostButton = document.getElementById('addPostButton');
		const modal = document.getElementById('modal');

		addPostButton.addEventListener('click', () => {
			modal.style.display = 'block';
		});

		// Закриття модального вікна при натисканні поза ним
		window.addEventListener('click', (event) => {
			if (event.target === modal) {
				modal.style.display = 'none';
			}
		});

		// Створення нової публікації при натисканні на кнопку "Створити"
		const createPostButton = document.getElementById('createPostButton');
		const postTitleInput = document.getElementById('postTitle');
		const postTextInput = document.getElementById('postText');

		createPostButton.addEventListener('click', () => {
			const title = postTitleInput.value;
			const body = postTextInput.value;
			const userId = 1; // ID користувача, якому присвоюється публікація

			// Створення об'єкта з даними публікації
			const newPost = {
				title,
				body,
				userId
			};

			// Відправка POST-запиту на створення нової публікації
			fetch('https://ajax.test-danit.com/api/json/posts', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(newPost)
			})
				.then(response => response.json())
				.then(data => {
					// Оновлення стрічки новин з новою публікацією
					createPostCard(data);

					// Очищення полів вводу
					postTitleInput.value = '';
					postTextInput.value = '';

					// Закриття модального вікна
					modal.style.display = 'none';
				});
		});
	});
